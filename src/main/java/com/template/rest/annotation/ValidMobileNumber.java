package com.template.rest.annotation;


import com.template.rest.annotation.validator.MobileNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {MobileNumberValidator.class}
)
public @interface ValidMobileNumber {

    //use messageCode in file "messages/messages.properties"
    //if want to add new message. have to put in all language
    String message() default "{error.invalid.mobile_number}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
