package com.template.rest.annotation;


import com.template.rest.annotation.validator.MobileNumberValidator;
import com.template.rest.annotation.validator.NotBlankWhenNotNull;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {NotBlankWhenNotNull.class}
)
public @interface NotBlankCostumed {

    String message() default "{javax.validation.constraints.NotBlank.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
