package com.template.rest.annotation.validator;

import com.template.rest.annotation.ValidMobileNumber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

@Slf4j
public class MobileNumberValidator implements ConstraintValidator<ValidMobileNumber, String> {

    private final Pattern patternOfMobileNumber = Pattern.compile("^(\\+\\d{2}\\d{9}$)");

//    @Override
//    public void initialize(ValidMobileNumber validMobileNumber) {}

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {

        boolean result;

        if (StringUtils.isEmpty(value)) {
            result = true;
        } else {
            result = patternOfMobileNumber.matcher(value).find();
        }

        return result;
    }
}