package com.template.rest.annotation.validator;

import com.template.rest.annotation.NotBlankCostumed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class NotBlankWhenNotNull implements ConstraintValidator<NotBlankCostumed, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {

        boolean result;

        if (value == null) {
            result = true;
        } else {
            result = !StringUtils.isEmpty(value);
        }

        return result;
    }
}