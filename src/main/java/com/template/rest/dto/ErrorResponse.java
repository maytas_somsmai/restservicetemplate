package com.template.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.template.rest.utility.GeneratorUtility;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private final String id = GeneratorUtility.generateTransactionId();
    private String code;
    private String message;
    private List<ErrorV1Field> errors = new ArrayList<>();

    public ErrorResponse(String code) {
        this.code = code;
    }

    public ErrorResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public void setErrors(List<ErrorV1Field> errors) {
        this.errors = errors;
    }

    public void addFieldError(String param, String code, String message) {
        ErrorV1Field error = new ErrorV1Field(code, param, message);
        errors.add(error);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ErrorV1Field implements Serializable {
        private String code;
        private String param;
        private String message;
    }
}
