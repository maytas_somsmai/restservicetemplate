package com.template.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BasicResponse<T> {
    private T data;
    private ErrorResponse error;

    public BasicResponse(T data) {
        this.data = data;
    }

    public BasicResponse(ErrorResponse error) {
        this.error = error;
    }
}
