package com.template.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.template.rest.annotation.ValidMobileNumber;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterRequest {


}
