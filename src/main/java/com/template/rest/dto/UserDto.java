package com.template.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDto {

    @NotNull
    private String firstName;

    private String middleName;

    @NotNull
    private String LastName;
}
