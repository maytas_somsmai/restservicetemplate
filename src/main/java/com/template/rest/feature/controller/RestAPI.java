package com.template.rest.feature.controller;

import com.template.rest.annotation.NotBlankCostumed;
import com.template.rest.annotation.ValidMobileNumber;
import com.template.rest.dto.BasicResponse;
import com.template.rest.dto.RegisterRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@Controller
@RequestMapping("/v1")
public class RestAPI {

    @GetMapping("/get/{mobile_number_1}")
    public ResponseEntity<BasicResponse> getMethods(
            @PathVariable(name = "mobile_number_1") @ValidMobileNumber String mobileNumber1,
            @RequestParam(name = "mobile_number_2") @ValidMobileNumber String mobileNumber2,
            @RequestParam(name = "mobile_number_3", required = false) @ValidMobileNumber @NotBlankCostumed String mobileNumber3
    ) {
        return new ResponseEntity<BasicResponse>(new BasicResponse("Success"), HttpStatus.OK);
    }

    @PostMapping("/post")
    public ResponseEntity<BasicResponse> postMethods(
            @RequestBody @Valid RegisterRequest request
    ) {
        return new ResponseEntity<BasicResponse>(new BasicResponse(request), HttpStatus.OK);
    }



}
