package com.template.rest.exception;

import com.google.common.base.CaseFormat;
import com.template.rest.dto.BasicResponse;
import com.template.rest.dto.ErrorResponse;
import com.template.rest.exception.code.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;
import javax.validation.ConstraintViolationException;

import static com.template.rest.exception.code.ErrorCode.INVALID_REQUEST_PARAM;
import static com.template.rest.exception.code.ErrorCode.REQUEST_PARAMS_MISSING;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler
    @ResponseBody
    //for GET method
    public ResponseEntity<BasicResponse> handle(MissingServletRequestParameterException e, ServletRequest request) {
        ErrorCode errorCode = REQUEST_PARAMS_MISSING;
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.addFieldError(e.getParameterName(), errorCode.name(),
                messageSource.getMessage("error.common.request.parameter.missing",null, request.getLocale())
        );
        return new ResponseEntity(new BasicResponse(errorResponse), errorCode.getHttpStatus());
    }

    @ExceptionHandler
    @ResponseBody
    //for GET method
    public ResponseEntity<BasicResponse> handle(ConstraintViolationException e) {
        ErrorCode errorCode = INVALID_REQUEST_PARAM;
        ErrorResponse errorResponse = new ErrorResponse();
        e.getConstraintViolations().forEach(c -> {
            String[] paramPath = c.getPropertyPath().toString().split("([.])");
            String paramName = paramPath[paramPath.length-1];
            errorResponse.addFieldError(paramName, errorCode.name(), c.getMessage());
        });
        return new ResponseEntity(new BasicResponse(errorResponse), errorCode.getHttpStatus());
    }

    @ExceptionHandler
    @ResponseBody
    //for POST method
    public ResponseEntity<BasicResponse> handle(MethodArgumentNotValidException e) {
        ErrorCode errorCode = INVALID_REQUEST_PARAM;
        ErrorResponse errorResponse = new ErrorResponse();
        e.getBindingResult().getFieldErrors().forEach( fieldError -> errorResponse.addFieldError(
                CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldError.getField()),
                errorCode.name(), fieldError.getDefaultMessage()));
        return new ResponseEntity(new BasicResponse(errorResponse), errorCode.getHttpStatus());
    }
}
