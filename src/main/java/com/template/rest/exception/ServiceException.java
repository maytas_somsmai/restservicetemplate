package com.template.rest.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.template.rest.exception.code.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceException extends RuntimeException{

    protected final HttpStatus httpStatus;
    protected final String errorCode;
    protected final String message;
    protected final Object[] params;

    public ServiceException(HttpStatus httpStatus, ErrorCode errorCode, String message, Object... params) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode.name();
        this.message = message;
        this.params = params;
    }

    public ServiceException(HttpStatus httpStatus, ErrorCode errorCode, String message) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode.name();
        this.message = message;
        this.params = new Object[]{};
    }
}
