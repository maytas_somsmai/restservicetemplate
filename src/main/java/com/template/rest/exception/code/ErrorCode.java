package com.template.rest.exception.code;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    INVALID_REQUEST_PARAM(HttpStatus.BAD_REQUEST),
    REQUEST_PARAMS_MISSING(HttpStatus.BAD_REQUEST);

    private final HttpStatus httpStatus;

    ErrorCode(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
