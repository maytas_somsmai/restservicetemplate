package com.template.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InputDTO {
    private String id;
    private String name;
    private String processTime;

    public InputDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
