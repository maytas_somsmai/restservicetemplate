package com.template.rest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AsyncTask.class, SpringAsyncConfig.class})
public class RestApplicationTests {

    @Autowired
    private AsyncTask asyncTask;

    @Test
    public void executeAsyncTask() throws IOException, InterruptedException, ExecutionException, TimeoutException {

        //get input file
        File fileInput = new File("src/test/resources/input.csv");
        CSVParser parser = CSVParser.parse(fileInput, Charset.forName("UTF-8"), CSVFormat.EXCEL.withFirstRecordAsHeader());

        //read file into data object
        List<InputDTO> inputList = new ArrayList<>();
        parser.forEach(record -> inputList.add(
                new InputDTO(record.get("id"), record.get("name")))
        );

        //let async tasks do they job
        List<CompletableFuture<InputDTO>> futureList = inputList.parallelStream().map(in -> {
            CompletableFuture<InputDTO> completableFuture = new CompletableFuture<>();
            try {
                completableFuture = asyncTask.asyncJob(in);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return completableFuture;
        }).collect(Collectors.toList());

        //collect all result
        CompletableFuture<Void> combinedFuture =
                CompletableFuture.allOf(futureList.toArray(new CompletableFuture[inputList.size()]));
        combinedFuture.get();

        //write to file
        FileWriter fileOutput = new FileWriter("src/test/resources/output.csv");
        CSVPrinter csvPrinter = new CSVPrinter(fileOutput, CSVFormat.EXCEL.withHeader("id", "name", "process_time"));
        futureList.forEach(result -> {
            try {
                InputDTO t = result.get();
                csvPrinter.printRecord(t.getId(), t.getName(), t.getProcessTime());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
        csvPrinter.flush();
    }

}
