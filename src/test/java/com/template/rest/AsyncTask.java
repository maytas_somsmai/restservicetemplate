package com.template.rest;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncTask {

    @Async("threadPoolTaskExecutor")
    public CompletableFuture<InputDTO> asyncJob(InputDTO inputDTO) throws InterruptedException {

        CompletableFuture<InputDTO> completableFuture = new CompletableFuture<>();

        long leftLimit = 800L;
        long rightLimit = 1200L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));

        if (generatedLong > 1000) {
            completableFuture.complete(new InputDTO(inputDTO.getId(), inputDTO.getName(), "ERROR"));
        }

        Thread.sleep(generatedLong);

        completableFuture.complete(new InputDTO(inputDTO.getId(), inputDTO.getName(), Long.toString(generatedLong)));

        return completableFuture;
    }

}
